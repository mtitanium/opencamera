
function addPhotoButtonClicked() {
    $.choosePhotoDialog.show();
}


function choosePhotoDialogClicked(e) {
    if (e.index === 0) {
        if (Ti.Media.hasCameraPermissions) {
            openCamera();
        } else {
            alert("No camera permission. Asking for Permission");
            Ti.Media.requestCameraPermissions(function(e) {
                // alert('request result'+JSON.stringify(e));
                if (e.success === true) {
                    openCamera();
                } else {
                    alert('خطأ في اعدادات الكاميرا');
                }
            });
        }
    } else if (e.index === 1) {
        Ti.Media.openPhotoGallery({
            allowEditing: true,
            mediaTypes: [Titanium.Media.MEDIA_TYPE_PHOTO],
            success: function(event) {
            	if(event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
					addToGalllery(event);
					
	                console.log(writeFile);
				} else {
					alert("got the wrong type back ="+event.mediaType);
				}
            },
            error: function(e) {
                // alert(e);
            }
        });
    }else{
        $.choosePhotoDialog.hide();
    }
}

function openCamera() {
	var overlay=createOverLay();
    Titanium.Media.showCamera({
        saveToPhotoGallery: true,
        success: function(event) {
            addToGalllery(event);
            // programatically hide the camera
            Ti.Media.hideCamera();
        },
        cancel: function() {},
        error: function(error) {
            var a = Titanium.UI.createAlertDialog({
                title: 'Camera'
            });
            if (error.code == Titanium.Media.NO_CAMERA) {
                //a.setMessage('Please run this test on device');
            } else {
                //a.setMessage(JSON.stringify(error));
            }
            a.setMessage('الرجاء ضبط اعدادات الكاميرا');
            a.show();
        },
        //overlay: overlay,
        //showControls: false, // don't show system controls
        mediaTypes: Ti.Media.MEDIA_TYPE_PHOTO,
        //autohide: false // tell the system not to auto-hide and we'll do it ourself
    });
}

function createOverLay () {
	var scanner = Titanium.UI.createView({
		width : 260,
		height : 200,
		//borderColor : 'red',
		borderWidth : 5,
		borderRadius : 15
	});

	var button = Titanium.UI.createImageView({
		color : '#fff',
		image : '/icons/dark_2x/dark_camera.png',
		backgroundImage : '/icons/dark_2x/dark_camera.png',
		backgroundSelectedImage : '/icons/dark_2x/dark_camera.png',
		backgroundDisabledImage : '/icons/dark_2x/dark_camera.png',
		bottom : 10,
		// width : 301,
		// height : 57,
		font : {
			fontSize : 20,
			fontWeight : 'bold',
			//fontFamily : 'Helvetica Neue'
		},
		//backgroundColor:Alloy.CFG.color,
	  	borderRadius: 5,
	  	//borderColor: Alloy.CFG.color,
		// title : 'التقاط صورة'
	});

	var messageView = Titanium.UI.createView({
		height : 30,
		width : 250,
		visible : false
	});

	var indView = Titanium.UI.createView({
		height : 30,
		width : 250,
		backgroundColor : '#000',
		borderRadius : 10,
		opacity : 0.7
	});
	messageView.add(indView);

	// message
	var message = Titanium.UI.createLabel({
		text : 'تم التقاط الصورة',
		color : '#fff',
		font : {
			fontSize : 20,
			fontWeight : 'bold',
			fontFamily : 'Helvetica Neue'
		},
		width : 'auto',
		height : 'auto'
	});
	messageView.add(message);

	var overlay = Titanium.UI.createView();
	overlay.add(scanner);
	overlay.add(button);
	overlay.add(messageView);

	button.addEventListener('click', function() {
		scanner.borderColor = 'blue';
		Ti.Media.takePicture();
		messageView.animate({
			visible : true
		});
		setTimeout(function() {
			scanner.borderColor = 'red';
			messageView.animate({
				visible : false
			});
		}, 500);
	});

	return overlay;
}

var imageKey=0;
var images=[];

function addToGalllery (event) {
    var fileName = 'reciept' + new Date().getTime() + '.png';
    writeFile = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory, fileName);
    writeFile.write(event.media);
    var imageUpladLoading=Alloy.Globals.loading;
    imageUpladLoading.show();
    // images[imageKey]=(Titanium.Platform.osname == 'android')?writeFile.read():writeFile;
    require('xhr').send({
		url: Alloy.CFG.baseurl+"product/upload",
		method: "POST",
		body: {
			image: (Titanium.Platform.osname == 'android')?writeFile.read():writeFile
		},
		success: function() {
			var callback = JSON.parse(this.responseText);
			console.log(callback);

			if (callback.error) {
			  	alert(callback.error);
                imageUpladLoading.hide();
			} else if (callback.success) {
				images[imageKey]=callback.image;
                var cellWidth = Alloy.CFG.per20;
                var cellHeight = Alloy.CFG.per20;
                var xSpacer = Alloy.CFG.per5;
                var ySpacer = Alloy.CFG.per2_5;
                //ySpacer='5';
                var xGrid = 2;

                var thisView = Ti.UI.createView({
                    layout: 'vertical',
                    borderRadius: 5,
                    borderColor: Alloy.CFG.color,
                    left: xSpacer,
                    height: cellHeight,
                    width: cellWidth,
                    top:ySpacer,
                    bottom:ySpacer,
                });
                // Create image view to hold profile pic
                var thisImage = Ti.UI.createImageView({
                    backgroundColor:"#FFFFFF",
                    top: 0,
                    // left: xSpacer,
                    height: cellHeight-50,
                    width: cellWidth,
                    image: callback.thumb,
                    // image: event.media,
                });
                var thisBtn = Ti.UI.createButton({
                    title: 'حذف',
                    width: Titanium.UI.FILL,
                    height: 50,
                    backgroundColor: Alloy.CFG.color,
                    top: 0,
                    color: "white",
                    font: {
                        fontSize: 16,
                        fontFamily: Alloy.Globals.customFont
                    }
                });
                thisBtn.addEventListener('click', function(e) {
                    imageUpladLoading.show()
                    require('xhr').send({
                        url: Alloy.CFG.baseurl+"product/deleteImage&customer_id"+Ti.App.Properties.getInt('customer_id'),
                        method: "POST",
                        body: {
                            image: images[imageKey]
                        },
                        success: function() {
                            imageUpladLoading.hide();
                            var callback = JSON.parse(this.responseText);
                            images.splice(imageKey, 1);
                            thisView.setWidth(0);
                            thisView.setVisible(false);
                        }
                    });
                    
                });
                $.addPhoto.setTitle('+ اضافة صورة اخرى');
                thisView.add(thisImage);
                thisView.add(thisBtn);
                $.gallery.add(thisView);
                imageKey++;
                imageUpladLoading.hide();
			} else {
			  	alert('خطا في الاتصال بالنترنت');
                imageUpladLoading.hide();
			}
		}
	});

	
}
function saveButtonClicked () {
	var name=$.productName.getValue().trim();
	var description=$.productDescription.getValue().trim();
	var contact=$.contact.getValue().trim();
	var price=$.price.getValue().trim();
	var location=$.location.getValue().trim();
	// alert(city_id+','+area_id+','+images.join()+','+product_category.join());

	if (!name.length||!description.length||!category_id_1||!zone_id) {
		alert('يرجى ادخال جميع البيانات');
	}else{
		Alloy.Globals.loading.show();
		require('xhr').send({
			url : Alloy.CFG.baseurl + "product/add&customer_id="+Ti.App.Properties.getInt('customer_id'),
			method : "POST",
			body : {
				name : name,
				description : description,
				contact : contact,
				price : price,
				country_id : 184,
				zone_id : zone_id,
				city_id : city_id,
				area_id : area_id,
				location : location,
				category_id_1 : category_id_1,
				category_id_2 : category_id_2,
				manufacturer_id : manufacturer_id,
				product_category : product_category.join(),
				images : images.join(),

			},
			success : function() {
				Alloy.Globals.loading.hide();
				var response = JSON.parse(this.responseText);
				var error = response.error;
				var success = response.success;
				console.log(response);
				if (error) {
					alert(error);
				} else if (success) {
					alert(success);
					Ti.App.Properties.refreshMyProducts=true;
					Alloy.Globals.pageStack.close($.getView());
				}else{
					alert('خطأ في الاتصال في الانترنت');
				}
			}
		});
	}
}